//
//  OkkoViewController.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class OkkoViewController: BaseTableViewController {

	let clientIdFieldId = "ClientId"
	let fielmIdFieldId = "fielmIdFieldId"

	var enteredClientId: String?
	var enteredFilmId: String?

	let manager = NetworkManager.sharedManager

	var textForTextView: String?

	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.title = "Okko"
		tableView.dataSource = self
		tableView.separatorStyle = .none
		tableView.register(UINib(nibName: String(describing: TextInputTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TextInputTableViewCell")
		tableView.register(UINib(nibName: String(describing: TwoButtonsTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TwoButtonsTableViewCell")
		tableView.register(UINib(nibName: String(describing: TextViewTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TextViewTableViewCell")
	}

	override func clearContentTapped() {
		textForTextView = ""
		enteredClientId = ""
		enteredFilmId = ""
		tableView.reloadData()
	}
}

extension OkkoViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 4
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextInputTableViewCell") as! TextInputTableViewCell
			cell.titleLabel.text = "Client ID"
			cell.fieldId = clientIdFieldId
			cell.textFieldd.text = enteredClientId
			cell.delegate = self
			return cell
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextInputTableViewCell") as! TextInputTableViewCell
			cell.titleLabel.text = "Film ID"
			cell.fieldId = fielmIdFieldId
			cell.textFieldd.text = enteredFilmId
			cell.delegate = self
			return cell
		case 2:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TwoButtonsTableViewCell") as! TwoButtonsTableViewCell
			cell.leftButton.setTitle("Watches movies", for: .normal)
			cell.rightButton.isUserInteractionEnabled = false
			cell.rightButton.alpha = 0
			cell.leftButtonAction = { self.didTapWhatchMovie() }
			return cell
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableViewCell") as! TextViewTableViewCell
			cell.titleLabel.text = "Kizen response"
			cell.fullTextLabel.text = textForTextView
			return cell
		}
	}

	func didTapWhatchMovie() {
		guard let clientId = enteredClientId, let filmId = enteredFilmId else {
			return
		}
		hud.show(in: self.view)
		manager.watchMovie(clientId: clientId, filmId: filmId) { (result) in
			self.hud.dismiss()
			self.textForTextView = result.value
			self.tableView.reloadData()
		}
	}
}

extension OkkoViewController: TextInputTableViewCellDelegate {
	func didChange(text: String, for fieldId: String) {
		switch fieldId {
		case clientIdFieldId:
			enteredClientId = text
		case fielmIdFieldId:
			enteredFilmId = text
		default: ()
		}
	}
}
