//
//  ListViewController.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 16.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	var chooseCompletionBlock: ((ListItem, String) -> Void)?
	private var items = [ListItem]()
	private var fieldId: String?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
		tableView.tableFooterView = UIView()
    }

	func update(with items: [ListItem], for fieldId: String?) {
		self.items = items
		self.fieldId = fieldId
	}
}

extension ListViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		items.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell")
		cell?.textLabel?.text = items[indexPath.row].title
		return cell ?? UITableViewCell()
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60
	}
}

extension ListViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let fieldId = fieldId else { return }
		chooseCompletionBlock?(items[indexPath.row], fieldId)
		navigationController?.popViewController(animated: true)
	}
}
