//
//  ListItem.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 16.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

struct ListItem {
	let title: String
}
