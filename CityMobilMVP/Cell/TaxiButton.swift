//
//  TaxiButton.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class TaxiButton: UIButton {

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	func setup() {
		tintColor = .white
		setTitleColor(.white, for: .normal)
		titleLabel?.textColor = .white
		titleLabel?.tintColor = .white
		backgroundColor = #colorLiteral(red: 0.3176470588, green: 0.4274509804, blue: 0.6705882353, alpha: 1)
		layer.cornerRadius = 5
		clipsToBounds = true
	}
}
