//
//  TwoButtonsTableViewCell.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class TwoButtonsTableViewCell: UITableViewCell {

	@IBOutlet weak var leftButton: UIButton!
	@IBOutlet weak var rightButton: UIButton!

	var leftButtonAction: (() -> Void)?
	var rightButtonAction: (() -> Void)?

	@IBAction func leftButtonTapped(_ sender: Any) {
		leftButtonAction?()
	}

	@IBAction func rightButtonTapped(_ sender: Any) {
		rightButtonAction?()
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	func setup() {
		selectionStyle = .none
	}
}
