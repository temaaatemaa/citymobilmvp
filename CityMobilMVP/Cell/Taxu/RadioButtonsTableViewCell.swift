//
//  RadioButtonsTableViewCell.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 16.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

enum RadioButtonsCellState: Int {
	case first = 0
	case second = 1
	case third = 2
}

class RadioButtonsTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var firstImageView: UIImageView!
	@IBOutlet weak var firstLabel: UILabel!
	@IBOutlet weak var secondImageView: UIImageView!
	@IBOutlet weak var secondLabel: UILabel!
	@IBOutlet weak var thirdImageView: UIImageView!
	@IBOutlet weak var thirdLabel: UILabel!

	var items = [ListItem]()
	var changeBlock: ((ListItem?, RadioButtonsCellState) -> Void)?

	func update(state: RadioButtonsCellState) {
		let offImage = UIImage(named: "off")
		let onImage = UIImage(named: "on")
		firstImageView.image = offImage
		secondImageView.image = offImage
		thirdImageView.image = offImage
		switch state {
		case .first:
			firstImageView.image = onImage
		case .second:
			secondImageView.image = onImage
		case .third:
			thirdImageView.image = onImage
		}
	}

	func update(selectedItem: ListItem) {
		let index = items.firstIndex { $0.title == selectedItem.title }
		guard let idx = index else { return }
		update(state: RadioButtonsCellState(rawValue: idx) ?? .first)
	}

	func update(items: [ListItem]) {
		guard items.count > 1, items.count <= 3 else { return }
		selectionStyle = .none
		self.items = items
		firstLabel.text = items.first?.title
		secondLabel.text = items[1].title
		if items.count == 3 {
			thirdLabel.text = items[2].title
			thirdLabel.isHidden = false
			thirdImageView.isHidden = false
		} else {
			thirdLabel.isHidden = true
			thirdImageView.isHidden = true
		}
	}

	@IBAction func firstButtonTapped(_ sender: Any) {
		update(state: .first)
		changeBlock?(items.first, .first)
	}

	@IBAction func secondButtonTapped(_ sender: Any) {
		update(state: .second)
		changeBlock?(items[1], .first)
	}
	@IBAction func thirdButtonTapped(_ sender: Any) {
		if items.count == 3 {
			update(state: .third)
			changeBlock?(items.last, .first)
		}
	}
	
}
