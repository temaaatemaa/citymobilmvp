//
//  TextInputTableViewCell.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

protocol TextInputTableViewCellDelegate: AnyObject {
	func didChange(text: String, for fieldId: String)
}
class TextInputTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var textFieldd: UITextField!
	@IBOutlet weak var downImageView: UIImageView!
	var dropDownState = false
	var fieldId: String?
	weak var delegate: TextInputTableViewCellDelegate?

	@IBAction func didChangeText(_ sender: Any) {
		delegate?.didChange(text: textFieldd.text ?? "", for: fieldId!)
	}
	@IBAction func diChangeEditing(_ sender: Any) {
		delegate?.didChange(text: textFieldd.text ?? "", for: fieldId!)
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	func setup() {
		selectionStyle = .none
	}

	func updateCell(dropDownState: Bool) {
		self.dropDownState = dropDownState
		if dropDownState {
			downImageView.isHidden = false
			textFieldd.isUserInteractionEnabled = false
		} else {
			downImageView.isHidden = true
			textFieldd.isUserInteractionEnabled = true
		}
	}
}
