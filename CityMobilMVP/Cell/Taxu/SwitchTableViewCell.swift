//
//  SwitchTableViewCell.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 15.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {

	@IBOutlet weak var switchLabel: UILabel!
	@IBOutlet weak var `switch`: UISwitch!

	var didChangeBlock: (() -> Void)?

	@IBAction func switchDidChange(_ sender: Any) {
		didChangeBlock?()
	}

}
