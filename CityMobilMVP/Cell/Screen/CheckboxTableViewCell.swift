//
//  CheckboxTableViewCell.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class CheckboxTableViewCell: UITableViewCell {

	@IBOutlet weak var checkboxView: UIView!
	@IBOutlet weak var checkboxText: UILabel!

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	func setup() {
		selectionStyle = .none
	}
}
