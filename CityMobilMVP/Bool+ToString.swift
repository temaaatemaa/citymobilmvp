//
//  Bool+ToString.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 16.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import Foundation

extension Bool {
	func toString() -> String { self ? "Yes" : "No" }
}

extension String {
	func toBool() -> Bool? {
		guard self == "Yes" || self == "NO" else { return nil }
		return self == "Yes"
	}
}
