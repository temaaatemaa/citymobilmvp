//
//  TaxiViewController.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class TaxiViewController: BaseTableViewController {

	static let destinationOptions = [
		ListItem(title: "Home"),
		ListItem(title: "Branch"),
		ListItem(title: "Airport")
	]
	static let boolOptions = [
		ListItem(title: "Yes"),
		ListItem(title: "No")
	]

	let clientIdFieldId = "ClientId"
	let timeOfATripFieldId = "timeOfATripFieldId"

	var enteredClientId: String?
	var enteredDestination = TaxiViewController.destinationOptions.first!
	var enteredTimeOfATrip: String?
	var branchOnTheWay = TaxiViewController.boolOptions.first!
	var longerThen15Min = TaxiViewController.boolOptions.first!

	let manager = NetworkManager.sharedManager

	var textForTextView: String?

	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.title = "Taxi"

		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.register(UINib(nibName: String(describing: TextInputTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TextInputTableViewCell")
		tableView.register(UINib(nibName: String(describing: TwoButtonsTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TwoButtonsTableViewCell")
		tableView.register(UINib(nibName: String(describing: TextViewTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TextViewTableViewCell")
		tableView.register(UINib(nibName: String(describing: RadioButtonsTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "RadioButtonsTableViewCell")
	}

	override func clearContentTapped() {
		textForTextView = ""
		enteredClientId = nil
		enteredTimeOfATrip = nil
		enteredDestination = TaxiViewController.destinationOptions.first!
		branchOnTheWay = TaxiViewController.boolOptions.first!
		longerThen15Min = TaxiViewController.boolOptions.first!
		tableView.reloadData()
	}

}

extension TaxiViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 7
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextInputTableViewCell") as! TextInputTableViewCell
			cell.titleLabel.text = "Client ID"
			cell.fieldId = clientIdFieldId
			cell.textFieldd.text = enteredClientId
			cell.textFieldd.keyboardType = .default
			cell.delegate = self
			cell.updateCell(dropDownState: false)
			return cell
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "RadioButtonsTableViewCell") as! RadioButtonsTableViewCell
			cell.titleLabel.text = "Destination"
			cell.update(items: TaxiViewController.destinationOptions)
			cell.update(selectedItem: enteredDestination)
			cell.changeBlock = { item, state in
				self.enteredDestination = item ?? self.enteredDestination
				self.view.endEditing(true)
			}
			return cell
		case 2:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextInputTableViewCell") as! TextInputTableViewCell
			cell.titleLabel.text = "Time of a trip in min"
			cell.fieldId = timeOfATripFieldId
			cell.textFieldd.text = enteredTimeOfATrip
			cell.textFieldd.keyboardType = .numberPad
			cell.delegate = self
			cell.updateCell(dropDownState: false)
			return cell
		case 3:
			let cell = tableView.dequeueReusableCell(withIdentifier: "RadioButtonsTableViewCell") as! RadioButtonsTableViewCell
			cell.titleLabel.text = "If the branch on the way"
			cell.update(items: TaxiViewController.boolOptions)
			cell.update(selectedItem: branchOnTheWay)
			cell.changeBlock = { item, state in
				self.branchOnTheWay = item ?? self.branchOnTheWay
				self.view.endEditing(true)
			}
			return cell
		case 4:
			let cell = tableView.dequeueReusableCell(withIdentifier: "RadioButtonsTableViewCell") as! RadioButtonsTableViewCell
			cell.titleLabel.text = "If stopover at the branch takes more then 15 minutes"
			cell.update(items: TaxiViewController.boolOptions)
			cell.update(selectedItem: longerThen15Min)
			cell.changeBlock = { item, state in
				self.longerThen15Min = item ?? self.longerThen15Min
				self.view.endEditing(true)
			}
			return cell
		case 5:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TwoButtonsTableViewCell") as! TwoButtonsTableViewCell
			cell.leftButton.setTitle("Call a taxi", for: .normal)
			cell.rightButton.setTitle("Got in a taxi", for: .normal)
			cell.leftButtonAction = { self.didTapCallTaxi() }
			cell.rightButtonAction = { self.didTapGotInTaxi() }
			return cell
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableViewCell") as! TextViewTableViewCell
			cell.titleLabel.text = "Kizen response"
			cell.fullTextLabel.text = textForTextView
			return cell
		}
	}

	func didTapCallTaxi() {
		view.endEditing(true)
		guard
			let clientId = enteredClientId,
			clientId.isEmpty == false,
			let timeOfATrip = enteredTimeOfATrip,
			timeOfATrip.isEmpty == false
		else { showAlert(); return }

		hud.show(in: self.view)
		manager.callTaxi(clientId: clientId,
						 destination: enteredDestination.title,
						 branchOnTheWay: branchOnTheWay.title.toBool() ?? false,
						 timeOfATrip: timeOfATrip,
						 longerThen15Mins: longerThen15Min.title.toBool() ?? false) { (result) in
							self.hud.dismiss()
			self.textForTextView = result.value
			self.tableView.reloadData()
		}
	}

	func didTapGotInTaxi() {
		view.endEditing(true)
		guard
			let clientId = enteredClientId,
			clientId.isEmpty == false,
			let timeOfATrip = enteredTimeOfATrip,
			timeOfATrip.isEmpty == false
		else { showAlert(); return }

		hud.show(in: self.view)
		manager.startTrip(clientId: clientId,
						  destination: enteredDestination.title,
						  branchOnTheWay: branchOnTheWay.title.toBool() ?? false,
						  timeOfATrip: timeOfATrip,
						  longerThen15Mins: longerThen15Min.title.toBool() ?? false) { (result) in
							self.hud.dismiss()
			self.textForTextView = result.value
			self.tableView.reloadData()
		}
	}

	func showAlert() {
		let alert = UIAlertController(title: "Error", message: "There is empty field", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
}

extension TaxiViewController: TextInputTableViewCellDelegate {
	func didChange(text: String, for fieldId: String) {
		switch fieldId {
		case clientIdFieldId:
			enteredClientId = text
		case timeOfATripFieldId:
			enteredTimeOfATrip = text
		default: ()
		}
	}
}
