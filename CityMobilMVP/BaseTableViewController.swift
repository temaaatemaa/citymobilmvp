//
//  ViewController.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class BaseTableViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!

//	var indicator: UIActivityIndicatorView!
	let hud = JGProgressHUD(style: .dark)

	override func viewDidLoad() {
		super.viewDidLoad()
		self.tableView.delegate = self

		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash,
																 target: self,
																 action: #selector(clearContentTapped))
	}

	@objc func clearContentTapped() {
		print("CLEAR")
	}
}

extension BaseTableViewController: UITableViewDelegate {
	public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.view.endEditing(true)
	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		self.view.endEditing(true)
	}
}
