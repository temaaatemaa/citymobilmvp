//
//  BaseResponseModel.swift
//  CityMobilMVP
//
//  Created by Valerii Korobov on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import Foundation

struct BaseResponseModel<T>: Codable where T: Codable {
    let body: [T]
    let errorCode: String
    let errorMessage: String

    init() {
        body = []
        errorCode = ""
        errorMessage = ""
    }

    enum CodingKeys: String, CodingKey {
        case body
        case errorCode
        case errorMessage
    }

}
