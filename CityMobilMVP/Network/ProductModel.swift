//
//  ProductModel.swift
//  CityMobilMVP
//
//  Created by Valerii Korobov on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductModel {
	var productId: String?
    var productName: String?
}

extension ProductModel: Mappable {
	init?(map: Map) {}

	mutating func mapping(map: Map) {
        productId  		<- map["productId"]
        productName   	<- map["productName"]
    }
}


