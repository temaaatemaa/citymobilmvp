//
//  NetworkManager.swift
//  CityMobilMVP
//
//  Created by Valerii Korobov on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//


import Foundation
import Alamofire
import ObjectMapper

struct API {
//https://apim.workato.com/dillond0/sber-v1/start-taxi-instant
	static let domenURL = "https://apim.workato.com/dillond0/sber-v1" // TODO: fill
	static let callTaxiURL = API.domenURL + "/call-taxi-instant" // TODO: fill
	static let startTripURL = API.domenURL + "/start-taxi-instant" // TODO: fill
	static let chooseProductURL = API.domenURL + "/choose-products" // TODO: fill
	static let watchMovieURL = API.domenURL + "/watch-movie-instant" // TODO: fill
}

class NetworkManager {

	static let sharedManager = NetworkManager()

	typealias StringCompletion = (Result<String>) -> Void
	typealias ProductListCompletion = (Result<[ProductModel]?>) -> Void

	private let errorDomain = "testDomain"
	private let manager: Alamofire.SessionManager = {
		let configuration = URLSessionConfiguration.default
		configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders

		return Alamofire.SessionManager(
			configuration: configuration
		)
	}()

    private func baseHeaders() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "API-TOKEN": "eaf257e0843adb47e86688d14475761adb302a537e67f6fc0c3c3deb39cc398d"
        ]
        return headers
    }

}

extension NetworkManager {

	func jsonToString(json: AnyObject) -> String? {
		do {
			let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
			let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
			return convertedString ?? ""
		} catch _ {
			return "Error"
		}
	}

	func callTaxi(clientId: String,
				  destination: String,
				  branchOnTheWay: Bool,
				  timeOfATrip: String,
				  militaryDateString: String = Date().military,
				  longerThen15Mins: Bool,
				  success: @escaping StringCompletion) {

		let params = [
			"clientId": clientId,
			"branchOnTheWay": branchOnTheWay,
			"destination": destination,
			"departureTime": militaryDateString,
			"longerThan15Mins": longerThen15Mins,
			"timeOfATrip": timeOfATrip,
		] as [String : Any]

		Alamofire.request(API.callTaxiURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: baseHeaders()).responseString { stringResponse in
			switch stringResponse.result {
			case .success(let string):
				success(Result.success(string))
			case .failure(let error):
				success(Result.success(error.localizedDescription))
			}
        }
	}

	func startTrip(clientId: String,
				   destination: String,
				   branchOnTheWay: Bool,
				   timeOfATrip: String,
				   militaryDateString: String = Date().military,
				   longerThen15Mins: Bool,
				   success: @escaping StringCompletion) {
		let params = [
			"clientId": clientId,
			"branchOnTheWay": branchOnTheWay,
			"destination": destination,
			"departureTime": militaryDateString,
			"longerThan15Mins": longerThen15Mins,
			"timeOfATrip": timeOfATrip,
		] as [String : Any]
		print(params)
		Alamofire.request(API.startTripURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: baseHeaders()).responseJSON { (jsonResponse) in
			switch jsonResponse.result {
			case .success(let json):
				let final = self.jsonToString(json: json as AnyObject)
				success(Result.success(final ?? ""))
			case .failure:
				success(Result.success("Error"))
			}
        }
	}

	func loadProducts(productName: String,
					   productId: String,
					   success: @escaping ProductListCompletion) {

		let domenURL = "https://demo7208108.mockable.io/chooseProduct" // TODO: fill

		Alamofire.request(domenURL, method: .get, parameters: nil, headers: nil).responseJSON { (jsonResponse) in

            switch jsonResponse.result {
            case .success(let value):

                guard let valueDict = value as? [String : Any],
                let resultJSON = valueDict["products"] as? [[String: Any]] else {
                        let error = NSError(domain:"", code:0, userInfo:nil)
						success(Result.failure(error))
                        return
                }

				let list = Mapper<ProductModel>().mapArray(JSONObject: resultJSON)
				success(Result.success(list))

            case .failure(let error):
				success(Result.failure(error))
            }
        }
	}

	func chooseProducts(clientid: String,
						products: [ProductModel],
						success: @escaping StringCompletion) {

		var productListJSON = [[String: String]]()
		var productJSON:[String: String] = [:]

		products.forEach { product in
			productJSON["productName"] = product.productName ?? ""
			productJSON["productId"] = product.productId ?? ""
			productListJSON.append(productJSON)
		}

		let params = [
			"clientid": clientid,
			"products": productListJSON
			] as [String : Any]
		print(params)

		Alamofire.request(API.chooseProductURL,
						  method: .post,
						  parameters: params,
						  encoding: JSONEncoding.default,
						  headers: baseHeaders()).responseString { jsonString in

							switch jsonString.result {
							case .success(let value):
								success(Result.success(value))
							case .failure(let error):
								success(Result.failure(error))
							}
		}
	}

	func watchMovie(clientId: String,
					filmId: String,
					success: @escaping StringCompletion) {
		let params = [
			"clientId": clientId,
			"filmId": filmId
			] as [String : Any]

		print(params)

		Alamofire.request(API.watchMovieURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: baseHeaders()).responseString { (stringResponse) in

			switch stringResponse.result {
			case .success(let value):
				success(Result.success(value))
			case .failure:
				success(Result.success("Error"))
			}
        }
	}
}

private extension NetworkManager {

	func printJsonResponse(jsonResponse: DataResponse<Any>) {
		#if DEBUG
		print("request = \(String(describing: jsonResponse.request))")
		print("json response = \(jsonResponse)")
		print("response = \(String(describing: jsonResponse.response))")
		print("response result = \(jsonResponse.result)")
		#endif
	}
}
