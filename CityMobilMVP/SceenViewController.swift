//
//  SceenViewController.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 01.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import UIKit

class SceenViewController: BaseTableViewController {

	var products = [ProductModel]()

	let manager = NetworkManager.sharedManager
	let clientIdFieldId = "ClientId"
	var enteredClientId: String?
	private var selectedProducts = [ProductModel]()

	override func viewDidLoad() {
		super.viewDidLoad()

		setupTableView()
		loadData()
		self.navigationItem.title = "Screen"
		self.navigationItem.rightBarButtonItem = nil
	}

	func updateWithStrings(products: [ProductModel]) {
		self.products = products
		tableView.reloadData()
	}

	func setupTableView() {
		tableView.allowsMultipleSelection = true
		tableView.dataSource = self
		tableView.delegate = self
		tableView.separatorStyle = .none
		tableView.register(UINib(nibName: String(describing: TextInputTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "TextInputTableViewCell")
		tableView.register(UINib(nibName: String(describing: CheckboxesTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "titleSubtitle")
		tableView.register(UINib(nibName: String(describing: CheckboxTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "checkbox")
		tableView.register(UINib(nibName: String(describing: ScreenButtonTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: "buton")
	}
}

extension SceenViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard !products.isEmpty else {return 0}
		return products.count + 2
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.row {

		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "TextInputTableViewCell") as! TextInputTableViewCell
			cell.titleLabel.text = "Client ID"
			cell.fieldId = clientIdFieldId
			cell.textFieldd.text = enteredClientId
			cell.textFieldd.keyboardType = .default
			cell.delegate = self
			cell.updateCell(dropDownState: false)
			return cell

		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "titleSubtitle") as! CheckboxesTableViewCell
			cell.titleLabel.text = "Kizen Stream"
			cell.subtitleLabel.text = "Choose the services you will need during your trip:"
			return cell
		case 2...products.count:
			let cell = tableView.dequeueReusableCell(withIdentifier: "checkbox") as! CheckboxTableViewCell
			cell.checkboxText.text = products[indexPath.row - 2].productName
			return cell
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "buton") as! ScreenButtonTableViewCell
			cell.button.setTitle("Get", for: .normal)
			cell.buttonAcionBlock = { self.didTapGetButton() }
			return cell
		}
	}

	func didTapGetButton() {
		makeRequestForInputData()
	}

}

extension SceenViewController {

	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		if let cell = tableView.cellForRow(at: indexPath) {
			if cell is CheckboxTableViewCell {
				(cell as! CheckboxTableViewCell).checkboxView.backgroundColor = .white
			}
		}
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		super.tableView(tableView, didSelectRowAt: indexPath)
		if let cell = tableView.cellForRow(at: indexPath) {
			if cell is CheckboxTableViewCell {
				(cell as! CheckboxTableViewCell).checkboxView.backgroundColor = #colorLiteral(red: 0.3176470588, green: 0.4274509804, blue: 0.6705882353, alpha: 1)
//				selectedProducts.
//				print(products)

				switch indexPath.row {
				case 2...products.count:
					self.selectedProducts.append(products[indexPath.row - 2])
					print(indexPath)
				default:
					return
				}
			}
		}
	}
}

extension SceenViewController: TextInputTableViewCellDelegate {
	func didChange(text: String, for fieldId: String) {
		enteredClientId = text
	}
}

extension SceenViewController {

	func loadData() {
		hud.show(in: self.view)
		manager.loadProducts(productName: "", productId: "") { (result) in
			self.hud.dismiss()
			switch result {
			case .success(let list):
				if let array = list {
					self.updateWithStrings(products: array as! [ProductModel])
				}
			case .failure(let _):
				print("")
			}
		}
	}

	func makeRequestForInputData() {
		guard
			let clientId = enteredClientId,
			!selectedProducts.isEmpty
		else { showAlert(); return }

		hud.show(in: self.view)
		manager.chooseProducts(clientid: clientId, products: selectedProducts) { result in
			self.hud.dismiss()
			switch result {
			case .success(let value):
				self.showAlert(title: "Success", message: value)
			case .failure(let error):
				self.showAlert(title: "Failure", message: error.localizedDescription)
			}
		}
	}

	func showAlert() {
		let alert = UIAlertController(title: "Error", message: "There is empty field", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}

	func showAlert(title: String, message: String) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
}
