//
//  Date+Military.swift
//  CityMobilMVP
//
//  Created by Artem Zabludovsky on 15.07.2020.
//  Copyright © 2020 n. All rights reserved.
//

import Foundation

extension Date {
	var military: String {
		let components = Calendar.current.dateComponents([.hour, .minute], from: self)
		guard let hour = components.hour, let minute = components.minute else { return "" }
		return String(format: "%02d%02d",
					  hour,
					  minute)
	}
}
